/**
 * 安全相关API
 */
import request,{Method} from '../utils/request'
import md5Libs from '../uview-ui/libs/function/md5.js'

/**
 * 发送绑定手机验证码
 * @param mobile
 * @param captcha
 * @param uuid
 */
export function sendBindMobileSms(mobile, captcha, uuid, scene) {
  return request.ajax({
    url: `/buyer/members/security/bind/send/${mobile}`,
    method: Method.POST,
    needToken: true ,
    loading:true,
    params: { uuid, captcha, scene}
  })
}

/**
 * 绑定手机号
 * @param mobile
 * @param sms_code
 */
export function bindMobile(mobile, sms_code) {
  return request.ajax({
    url: `/buyer/members/security/bind/${mobile}`,
    method: Method.PUT,
    needToken: true ,
    params: { sms_code }
  })
}

/**
 * 发送手机验证码
 * 在修改手机号和更改密码时通用
 * @param uuid
 * @param captcha
 */
export function sendMobileSms(uuid, captcha, scene) {
  return request.ajax({
    url: '/buyer/members/security/send',
    method: Method.POST,
    needToken: true ,
    loading:true,
    params: { uuid, captcha , scene}
  })
}

/**
 * 验证更换手机号短信
 * @param sms_code
 */
export function validChangeMobileSms(sms_code) {
  return request.ajax({
    url: '/buyer/members/security/exchange-bind',
    method: Method.GET,
    needToken: true ,
    loading:true,
    params: { sms_code }
  })
}

/**
 * 更换手机号
 * @param mobile
 * @param sms_code
 */
export function changeMobile(mobile, sms_code) {
  return request.ajax({
    url: `/buyer/members/security/exchange-bind/${mobile}`,
    method: Method.PUT,
    needToken: true ,
    loading:true,
    params: { sms_code }
  })
}

/**
 * 验证更改密码手机短信
 * @param sms_code
 */
export function validChangePasswordSms(sms_code) {
  return request.ajax({
    url: '/buyer/members/security/password',
    method: Method.GET,
    needToken: true ,
    loading:true,
    params: { sms_code }
  })
}

/**
 * 更改密码
 * @param uuid
 * @param captcha
 * @param password
 */
export function changePassword(uuid, captcha, password, scene) {
  return request.ajax({
    url: '/buyer/members/security/password',
    method: Method.PUT,
    needToken: true ,
    loading:true,
    params: {
      uuid,
      captcha,
      password: md5Libs.md5(password),
      scene
    }
  })
}

/**
 * 发送手机验证码
 * 修改支付密码时用
 * @param params
 */
export function sendMobileSmsForPayment(params) {
  return request.ajax({
    url: '/buyer/members/wallet/smscode',
    method: Method.POST,
    needToken: true,
    params
  })
}

/**
 * 设置修改支付密码
 * @param params
 */
export function bindPaymentPassword(params) {
  params.password = md5Libs.md5(params.password)
  return request.ajax({
    url: `/buyer/members/wallet/set-pwd`,
    method: Method.POST,
    needToken: true,
    params
  })
}

/**
 * 验证更改支付密码手机短信
 * @param sms_code
 */
export function validChangePaymentPasswordSms(sms_code) {
  return request.ajax({
    url: '/buyer/members/wallet/check/smscode',
    method: Method.GET,
    needToken: true,
    params: { sms_code }
  })
}

/**
 * 发送注销手机验证码
 * @param uuid
 * @param captcha
 */
export function sendLogoffMobileSms(params) {
  return request.ajax({
    url: '/buyer/members/log-off/send',
    method: Method.POST,
    needToken: true ,
    loading:true,
    params
  })
}

/**
 * 账号注销
 * @param sms_code
 */
export function membersLogOff(sms_code) {
  return request.ajax({
    url: '/buyer/members/log-off',
    method: Method.POST,
    needToken: true ,
    loading:true,
    params: { sms_code }
  })
}